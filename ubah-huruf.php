<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
<?php
    function ubah_huruf($string){
        //kode di sini
        $hasil = "";
        for ($i = 0; $i < strlen($string); $i++) {
            $hasil .= chr(ord(substr($string,$i,1))+1);
        }
        return $hasil;
    }
    
    // TEST CASES
    echo ubah_huruf('wow')."<br>"; // xpx
    echo ubah_huruf('developer')."<br>"; // efwfmpqfs
    echo ubah_huruf('laravel')."<br>"; // mbsbwfm
    echo ubah_huruf('keren')."<br>"; // lfsfo
    echo ubah_huruf('semangat')."<br>"; // tfnbohbu
    
?>
</body>
</html>